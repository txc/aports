# Maintainer: psykose <alice@ayaya.dev>
pkgname=gst-plugins-rs
pkgver=0.10.4
pkgrel=0
pkgdesc="Gstreamer rust plugins"
url="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs"
# ppc64le, s390x, riscv64: ring
arch="all !ppc64le !s390x !riscv64"
license="MIT AND Apache-2.0 AND MPL-2.0 AND LGPL-2.1-or-later"
makedepends="
	cargo
	cargo-c
	dav1d-dev
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gtk4.0-dev
	libsodium-dev
	meson
	nasm
	openssl-dev
	"
subpackages="$pkgname-dev $pkgname-tools"
source="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/archive/$pkgver/gst-plugins-rs-$pkgver.tar.gz
	cargo-upd.patch
	dylib.patch
	"
options="net !check" # they don't run

export SODIUM_USE_PKG_CONFIG=1
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	export CARGO_PROFILE_RELEASE_OPT_LEVEL=2
	abuild-meson \
		--buildtype=release \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

tools() {
	amove usr/bin
}

sha512sums="
cc7546baf5825292d156650d412ff7ab050dee7ea46117e49c6db3bbed5ab9e34743d88c1f29ec0faf1fdcbbedf10e27885654641c2e84a7fb1d68440974ed29  gst-plugins-rs-0.10.4.tar.gz
bf761f6b459c5732d0bb72489bc877cfc9a93fa903bd816d4be8f06a40d733e03496f05620a2f62df8cd7a6f9716ab14e9dbdf91cd16a6e0f3dc2a52a3615924  cargo-upd.patch
5f354a7776859f62a235947b5a31779688bc681f3c47c3fbf85806c8a12f68023a731067c958e40dfd580af591ea271e4e5184ef3b45a193c9b855486c64fef0  dylib.patch
"
