# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=hyperfine
pkgver=1.16.0
pkgrel=0
pkgdesc="Command-line benchmarking tool"
url="https://github.com/sharkdp/hyperfine"
# s390x: nix statfs broken with musl
arch="all !s390x"
license="Apache-2.0 AND MIT"
makedepends="cargo"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/sharkdp/hyperfine/archive/v$pkgver/$pkgname-$pkgver.tar.gz"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

export CARGO_PROFILE_RELEASE_OPT_LEVEL="z"
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test all --frozen
}

package() {
	install -Dm755 target/release/hyperfine \
		"$pkgdir"/usr/bin/hyperfine
	install -Dm644 doc/hyperfine.1 \
		"$pkgdir"/usr/share/man/man1/hyperfine.1

	install -Dm644 target/release/build/hyperfine-*/out/hyperfine.bash \
		"$pkgdir/usr/share/bash-completion/completions/hyperfine"
	install -Dm644 target/release/build/hyperfine-*/out/_hyperfine \
		"$pkgdir/usr/share/zsh/site-functions/_hyperfine"
	install -Dm644 target/release/build/hyperfine-*/out/hyperfine.fish \
		"$pkgdir/usr/share/fish/completions/hyperfine.fish"
}

sha512sums="
1b28ca7420751d758f1a6673f4d7c034a5b0720d6d011fc542a9f304c03f1a161d386389877341b41b9a4e10bf79bc6fda04e0a42479bf9b644731d31048eb1a  hyperfine-1.16.0.tar.gz
"
