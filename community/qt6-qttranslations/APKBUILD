# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qttranslations
pkgver=6.4.3
pkgrel=0
pkgdesc="A cross-platform application and UI framework (Translations)"
url="https://qt.io/"
arch="noarch"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
makedepends="
	cmake
	perl
	qt6-qtbase-dev
	qt6-qttools-dev
	samurai
	"
builddir="$srcdir/qttranslations-everywhere-src-${pkgver/_/-}"
options="!check" # No tests

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qttranslations-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	# -j1 to prevent race conditions and missing translation files
	cmake --build build -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
200dd7418263474be95de25023ddd881f1cbc46fe4a0767aa33733414564aef9615b36429f511698d012704c4e94088026b3d9e454d9078e8975b48119c45c0d  qttranslations-everywhere-src-6.4.3.tar.xz
"
