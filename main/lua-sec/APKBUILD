# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
_luaversions="5.1 5.2 5.3 5.4"
pkgname=lua-sec
pkgver=1.3.0
pkgrel=0
pkgdesc="TLS/SSL Support for Lua"
url="https://github.com/brunoos/luasec/wiki"
arch="all"
license="MIT"
makedepends="openssl-dev>3"
subpackages="$pkgname-doc"
for _i in $_luaversions; do
	makedepends="$makedepends lua$_i-dev"
	subpackages="$subpackages lua$_i-sec:_split"
done

source="luasec-$pkgver.tar.gz::https://github.com/brunoos/luasec/archive/v$pkgver.tar.gz"

builddir="$srcdir/luasec-$pkgver"

prepare() {
	default_prepare
	for _i in $_luaversions; do
		cp -r "$builddir" "$srcdir"/build-$_i
	done
}

build() {
	for _i in $_luaversions; do
		cd "$srcdir"/build-$_i
		make linux INC_PATH="$(pkg-config --cflags lua$_i)" \
			LD="${CC:-gcc}"
	done
}

package() {
	for _i in $_luaversions; do
		local _luasharedir=/usr/share/lua/$_i
		local _lualibdir=/usr/lib/lua/$_i
		cd "$srcdir"/build-$_i
		mkdir -p "$pkgdir"/$_luasharedir "$pkgdir"/$_lualibdir
		make LUAPATH="$pkgdir"/$_luasharedir \
			LUACPATH="$pkgdir"/$_lualibdir \
			install
		if ! [ -d "$pkgdir"/usr/share/doc/$pkgname/sambles ]; then
			mkdir -p "$pkgdir"/usr/share/doc/$pkgname
			cp -r samples "$pkgdir"/usr/share/doc/$pkgname/
		fi
	done
}

_split() {
	local d= _ver=${subpkgname#lua}
	_ver=${_ver%-*}
	pkgdesc="TLS/SSL Support for Lua $_ver"
	install_if="lua$_ver $pkgname=$pkgver-r$pkgrel"
	replaces="$pkgname"
	depends="lua$_ver-socket"
	for d in usr/lib/lua usr/share/lua; do
		if [ -d "$pkgdir"/$d/$_ver ]; then
			mkdir -p "$subpkgdir"/$d
			mv "$pkgdir"/$d/$_ver "$subpkgdir"/$d/
		fi
	done
}

sha512sums="
5dcaca773ad6ee00ca33cfcc2060c763a8c75ad45753a4e5f11e6e76bdb2a49a8b46780efec12146f691f900326218e63c11af5abb709ca84c16bb77ce791943  luasec-1.3.0.tar.gz
"
